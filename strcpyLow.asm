          global  strcpyLow
          section .text
strcpyLow:

loop:
         mov        al, byte[rsi]
         mov        byte[rdi],al
         cmp        al,0
         jz         end
         inc        rsi
         inc        rdi
         jmp        loop
end:
           ret

