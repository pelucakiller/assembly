          global  strcpyHigh
          section .text
strcpyHigh:

loop:
         mov        ah, byte[rsi]
         mov        byte[rdi],ah
         cmp        ah,0
         jz         end
         inc        rsi
         inc        rdi
         jmp        loop

end:
           ret
