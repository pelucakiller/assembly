#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define BUFFER_SIZE 1024 * 1024 * 512
//#define BUFFER_SIZE 32

void strcpyLow(char *, char *);
void strcpyHigh(char *, char *);
void strNcpy(char *, char *, int);

/*
nasm -felf64 strcpyLow.asm \
&& nasm -felf64 strcpyHigh.asm \
&& nasm -felf64 strNcpy.asm \
&& gcc -O0 -ggdb clockAsmStrncpyStrcpy.c strcpyHigh.o strcpyLow.o strNcpy.o -o clockAsmStrncpyStrcpy \
&& rm *.o \
&& ./clockAsmStrncpyStrcpy
*/

int main(int argc, char * argv[]) {
   char * source = malloc(BUFFER_SIZE);
   char * target = malloc(BUFFER_SIZE);
   if ( source == 0 || target == 0) {
     return 1;
   }

   memset(source, 'a', BUFFER_SIZE - 2);
   *(source + BUFFER_SIZE - 2) = 'X';
   *(source + BUFFER_SIZE - 1) = 0;


   clock_t start, end;
   double cpu_time_used;
   int times;

   printf("source: ...%s\n", source+BUFFER_SIZE-16);

   printf("\nstrcpyLow\n");
   for (times = 0 ; times < 10; ++times) {
     start = clock();
     strcpyLow(target, source);
     end = clock();
     cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
     printf("%f\n", cpu_time_used);
   }

   printf("target: ...%s\n", source+BUFFER_SIZE-16);
   memset(target, '.', BUFFER_SIZE);

   printf("\nstrcpyHigh\n");
   for (times = 0 ; times < 10; ++times) {
     start = clock();
     strcpyHigh(target, source); 
     end = clock();
     cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
     printf("%f\n", cpu_time_used);
   }

   printf("target: ...%s\n", source+BUFFER_SIZE-16);

   printf("\nstrNcpy\n");
   for (times = 0 ; times < 10; ++times) {
     start = clock();
     strNcpy(target, source, BUFFER_SIZE); 
     end = clock();
     cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
     printf("%f\n", cpu_time_used);
   }

   printf("target: ...%s\n", source+BUFFER_SIZE-16);
   memset(target, '.', BUFFER_SIZE);

   printf("\nstrcpy\n");
   for (times = 0 ; times < 10; ++times) {
     start = clock();
     strcpy(target, source);
     end = clock();
     cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
     printf("%f\n", cpu_time_used);
   }


   printf("target: ...%s\n", source+BUFFER_SIZE-16);
   memset(target, '.', BUFFER_SIZE);

   printf("\nstrncpy\n");
   for (times = 0 ; times < 10; ++times) {
     start = clock();
     strncpy(target, source, BUFFER_SIZE);
     end = clock();
     cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
     printf("%f\n", cpu_time_used);
   }

   printf("target: ...%s\n", source+BUFFER_SIZE-16);


   free(source);
   free(target);
}
