          global  strNcpy
          section .text
strNcpy:

loop:
         cmp        rdx, 0
         jz         end
         dec        rdx
         mov        al, byte[rsi]
         mov        byte[rdi],al
         cmp        al,0
         jz         end
         inc        rsi
         inc        rdi
         jmp        loop
end:
           ret

