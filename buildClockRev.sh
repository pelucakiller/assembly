nasm -felf64 strcpyLow.asm \
&& nasm -felf64 strcpyHigh.asm \
&& nasm -felf64 strNcpy.asm \
&& gcc -O0 -ggdb clockAsmStrncpyStrcpy.c strcpyHigh.o strcpyLow.o strNcpy.o -o clockedRev.bin \
&& rm *.o \
&& ./clockedRev.bin
