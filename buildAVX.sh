nasm -felf64 strcpyAVX.asm \
&& gcc -O0 -ggdb testAVXCall.c strcpyAVX.o -o avx.bin \
&& rm *.o \
&& ./avx.bin
