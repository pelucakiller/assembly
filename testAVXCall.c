#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define BUFFER_SIZE 1024 * 1024 * 512
#define BUFFER_SIZE 64

void strcpyAVX(char *, char *, char *);

/*
*/

int main(int argc, char * argv[]) {
   char * source = malloc(BUFFER_SIZE);
   char * target = malloc(BUFFER_SIZE);
   char * buffer = malloc(32);
   if ( source == 0 || target == 0 || buffer == 0) {
     return 1;
   }

   memset(source, 'a', BUFFER_SIZE - 2);
   *(source + BUFFER_SIZE - 2) = 'X';
   *(source + BUFFER_SIZE - 1) = 0;


   clock_t start, end;
   double cpu_time_used;
   int times;

   printf("source: ...%s\n", source + BUFFER_SIZE-16);

   printf("\nstrcpyLow\n");
   for (times = 0 ; times < 10; ++times) {
     start = clock();
     strcpyAVX(target, source, buffer);
     end = clock();
     cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
     printf("%f\n", cpu_time_used);
   }

   printf("target: ...%s\n", target + BUFFER_SIZE-16);
   printf("target: ...%s\n", target) ;

   free(source);
   free(target);
   free(buffer);
}
