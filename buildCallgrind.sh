nasm -felf64 strcpyLow.asm \
&& nasm -felf64 strcpyHigh.asm \
&& nasm -felf64 strNcpy.asm \
&& gcc -O0 -ggdb callgrindStrncpyStrcpyAsm.c strcpyHigh.o strcpyLow.o strNcpy.o -o callgrined.bin \
&& rm *.o \
&& valgrind --tool=callgrind ./callgrined.bin \
&& echo kcachegrind callgrind.out.???
