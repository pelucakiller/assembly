#include <malloc.h>
#include <string.h>

#define BUFFER_SIZE 1024 * 1024 * 512
#define BUFFER_SIZE 32

void strcpyLow(char *, char *);
void strcpyHigh(char *, char *);
void strNcpy(char *, char *, int);

/*
nasm -felf64 strcpyLow.asm \
&& nasm -felf64 strcpyHigh.asm \
&& nasm -felf64 strNcpy.asm \
&& gcc -O0 -ggdb testStrncpyStrcpyAsm.c strcpyHigh.o strcpyLow.o strNcpy.o \
&& rm *.o \
&& valgrind --tool=callgrind ./a.out \
&& echo kcachegrind callgrind.out.???

*/

int main(int argc, char * argv[]) {
   char * source = malloc(BUFFER_SIZE);
   char * target = malloc(BUFFER_SIZE);
   if ( source == 0 || target == 0) {
     return 1;
   }

   memset(source, 'a', BUFFER_SIZE - 2);
   *(source + BUFFER_SIZE - 2) = 'X';
   *(source + BUFFER_SIZE - 1) = 0;

   int times;

   for (times = 0 ; times < 10; ++times) {
     strcpy(target, source);
   }

   memset(target, '.', BUFFER_SIZE);

   for (times = 0 ; times < 10; ++times) {
     strncpy(target, source, BUFFER_SIZE);
   }

   memset(target, '.', BUFFER_SIZE);

   for (times = 0 ; times < 10; ++times) {
     strcpyLow(target, source); 
   }

   memset(target, '.', BUFFER_SIZE);

   for (times = 0 ; times < 10; ++times) {
     strcpyHigh(target, source);
   }

   memset(target, '.', BUFFER_SIZE);

   for (times = 0 ; times < 10; ++times) {
     strNcpy(target, source, BUFFER_SIZE);
   }

   printf("strcpyLow  : %p\n", strcpyLow);
   printf("strcpyHigh : %p\n", strcpyHigh);
   printf("strNcpy    : %p\n", strNcpy);


   free(source);
   free(target);
}
