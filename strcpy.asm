; nasm -felf64 strcpy.asm && gcc strcpy.o && ./a.out
         global    main

         extern     printf

         section    .data
source:  db         "Hello, World",0
target1: db         "............",0
target2: db         "............",0
format:  db         "%s",10,0

         section   .text
main:

        mov         rdi, format
        mov         rsi, source
        xor         rax,rax
        call        printf

        mov         rdi, format
        mov         rsi, target1
        xor         rax,rax
        call        printf

        mov         rdi, format
        mov         rsi, target2
        xor         rax,rax
        call        printf

strcpy:
         mov        rsi, source
         mov        rdi, target1
loop1:
         mov        al, byte[rsi]  ;   4
         mov        byte[rdi],al   ;   2
         cmp        al,0           ;   1
         jz         next           ;   2
         inc        rsi            ;   1
         inc        rdi            ;   1
         jmp        loop1          ;   1
; total                                12 (20% faster)

next:
strncpy:
         mov        rbx, 08
         mov        rsi, source
         mov        rdi, target2

loop2:
         cmp        rbx, 0         ;   1
         je         printing       ;   2
         dec        rbx            ;   1
         mov        al, byte[rsi]  ;   4
         mov        byte[rdi],al   ;   2
         cmp        al,0           ;   1
         jz         printing       ;   1
         inc        rsi            ;   1
         inc        rdi            ;   1
         jmp        loop2          ;   1
; total                               15

printing:


        mov         rdi, format
        mov         rsi, source
        xor         rax,rax
        call        printf

        mov         rdi, format
        mov         rsi, target1
        xor         rax,rax
        call        printf

        mov         rdi, format
        mov         rsi, target2
        xor         rax,rax
        call        printf

exit:
         mov        rax, 60
         xor        rdi, rdi
         syscall
