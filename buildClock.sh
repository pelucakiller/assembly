nasm -felf64 strcpyLow.asm \
&& nasm -felf64 strcpyHigh.asm \
&& nasm -felf64 strNcpy.asm \
&& gcc -O0 -ggdb clockStrncpyStrcpyAsm.c strcpyHigh.o strcpyLow.o strNcpy.o -o clocked.bin \
&& rm *.o \
&& ./clocked.bin
