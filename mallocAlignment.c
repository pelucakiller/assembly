#include <malloc.h>
#include <stdio.h>
#include <string.h>


//#define BUFFER_SIZE 1024 * 1024 * 512
//#define BUFFER_SIZE 32
#define BUFFER_SIZE 32
#define ALIGNMENT 8


/*

gcc -O0 -ggdb mallocAlignment.c -o mallocAlignment \
&& ./mallocAlignment

*/

int main(int argc, char * argv[]) {
   char * dummy = malloc(4);
   char * source = malloc(BUFFER_SIZE + ALIGNMENT);
   char * alignedSource;

   if ( source == 0 || dummy == 0) {
     return 1;
   }

   printf("%p\n", dummy);
   printf("%p\n", source);

   for (int i=1; i < 1000; i*=2) {
      //printf("%d %d\n", i, ((void *) source) % i) ;
   }
  free(source);
  free(dummy);
}
