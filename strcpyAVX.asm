;  incomplete
          global  strcpyAVX
          section .text

; rdi source
; rsi target
; rdx buffer 
strcpyAVX:
    
    vpbroadcastb ymm1, '0'    ; load zeros to comparisson register ( _mm256_set1_epi8 )
loop:
    vmovdqu8 ymm0, [rdi]    ; load from source

    vpcmpeqb  ymm3, ymm1, ymm0    ; comparar ( _mm256_cmpeq_epi8 )
    ;setnz dil
    ;movzx edx, dil
    ; si hay un cero saltar a ultimo_bloque
    ; descargar los 32 bytes en destino
    vmovdqu8 [rsi], ymm0    ; store to target
    ret
    add rdi, 32           ; incrementar origen
    add rsi, 32           ; incrementar destino
    jmp loop

ultimo_bloque:
    ; descargar los 32 bytes a espacio intermedio

loop2:
    ; copiar un byte de intermedio a destino
    ; si intermedio es cero saltar a finalizar
    inc rdx               ; incrementar intermedio
    inc rdi               ; incrementar destino
    jmp loop2

finalizar: 
    ret
