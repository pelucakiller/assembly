nasm -felf64 strcpyLow.asm \
&& nasm -felf64 strcpyHigh.asm \
&& nasm -felf64 strNcpy.asm \
&& gcc -O0 -ggdb -static clockStrncpyStrcpyAsm.c strcpyHigh.o strcpyLow.o strNcpy.o -o staticClocked.bin \
&& rm *.o \
&& ./staticClocked.bin
